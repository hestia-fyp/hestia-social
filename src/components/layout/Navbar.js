import React, { Component, Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import MyButton from '../../util/MyButton';
import PostScream from '../scream/PostScream';
import Notifications from './Notifications';
// MUI stuff
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
// Icons
import HomeIcon from '@material-ui/icons/Home';
import ChatIcon from '@material-ui/icons/Chat';
import TodoIcon from '@material-ui/icons/AssignmentTurnedIn';
import Bug from '@material-ui/icons/BugReport';



class Navbar extends Component {
  render() {
    const { authenticated } = this.props;
    return (
      <AppBar>
        <Toolbar className="nav-container" style={{justifyContent:"center", width:"100%"}}>
          <div style={{ fontWeight:"bold", fontSize:"1.5rem", marginRight:"50%"}}>hestia</div>
          {authenticated ? (
            <Fragment>
              <PostScream />
              <Link to="/">
                <MyButton tip="Home">
                  <HomeIcon style={{filter:"invert(50)"}}/>
                </MyButton>
              </Link>
              <Button color="inherit" component = {Link} to={{ pathname: "http://localhost:7000" }} target="_blank" style={{filter:"invert(50)"}}>
                <ChatIcon/>
              </Button>
              <Button color="inherit" component = {Link} to={{ pathname: "http://localhost:5000" }} target="_blank" style={{filter:"invert(50)"}}>
                <TodoIcon/>
              </Button>
              <Notifications/>
              <Button color="inherit" component = {Link} to={{ pathname: "http://localhost:4000" }} target="_blank" style={{filter:"invert(50)"}}>
                <Bug/>
              </Button>
            </Fragment>
          ) : (
            <Fragment>
              <Button color="inherit" component={Link} to="/login">
                Login
              </Button>
              <Button color="inherit" component={Link} to="/">
                Home
              </Button>
              <Button color="inherit" component={Link} to="/signup">
                Signup
              </Button>
            </Fragment>
          )}
        </Toolbar>
      </AppBar>
    );
  }
}

Navbar.propTypes = {
  authenticated: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated
});

export default connect(mapStateToProps)(Navbar);
